# Biocblast

This package is a simple helper that interfaces R/Bioconductor with
the blast suite of tools from NCBI.

# License

GPL-3

<!-- Local Variables: -->
<!-- mode: markdown -->
<!-- End: -->
