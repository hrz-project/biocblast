
.check_percent <- function(x) {
  x > 0 && x <= 100
}

.check_best_hit <- function(x) {
  x > 0 && x < 0.5
}

blastRestrictOptions <- function(perc_identity, qcov_hsp_perc,
                                 max_hsps, culling_limit,
                                 best_hit_overhang,
                                 best_hit_score_edge, subject_besthit,
                                 max_target_seqs)
{
  obj <- new("BlastRestrictOptions")
  if (!(missing(max_target_seqs))) slot(obj, "max_target_seqs") <- max_target_seqs
  if (!(missing(subject_besthit))) slot(obj, "subject_besthit") <- subject_besthit
  if (!(missing(best_hit_score_edge))) slot(obj, "best_hit_score_edge") <- best_hit_score_edge
  if (!(missing(best_hit_overhang))) slot(obj, "best_hit_overhang") <- best_hit_overhang
  if (!(missing(culling_limit))) slot(obj, "culling_limit") <- culling_limit
  if (!(missing(max_hsps))) slot(obj, "max_hsps") <- max_hsps
  if (!(missing(qcov_hsp_perc))) slot(obj, "qcov_hsp_perc") <- qcov_hsp_perc
  if (!(missing(perc_identity))) slot(obj, "perc_identity") <- perc_identity
  ##
  if (validObject(obj)) obj
}

setValidity("BlastRestrictOptions", function(object) {
  msg <- NULL
  empty <- function(x) length(x) == 0
  if (!(empty(object@perc_identity) || .check_percent(object@perc_identity)))
    msg <- c(msg, "perc_identity should be between 0 and 100")
  if (!(empty(object@qcov_hsp_perc) || .check_percent(object@qcov_hsp_perc)))
    msg <- c(msg, "qcov_hsp_perc should be between 0 and 100")
  if (!(empty(object@max_hsps) || object@max_hsps >= 1))
    msg <- c(msg, "max_hsps should be at least 1.")
  if (!(empty(object@culling_limit) || object@culling_limit >= 0 ))
    msg <- c(msg, "culling_limit should be positive.")
  if (!(empty(object@best_hit_overhang) || .check_best_hit(object@best_hit_overhang)))
    msg <- c(msg, "Overhang should be between 0 and 0.5")
  if (!(empty(object@best_hit_score_edge) || .check_best_hit(object@best_hit_score_edge)))
    msg <- c(msg, "Best hit score edge should be between 0 and 0.5")
  if (!(empty(object@subject_besthit) || is.logical(object@subject_besthit)))
    msg <- c(msg, "subject_besthit should be boolean")
  if (!(empty(object@max_target_seqs) || object@max_target_seqs >= 1))
    msg <- c(msg, "max_target_seqs should be at least 1")
  ## TODO implement gilist and taxidlist restricting here.
  if (empty(msg)) TRUE else msg
})

setMethod("show", "BlastRestrictOptions",
          function(object) {
            .showObjectAsSimpleList(object, "Restrictions on blast results")
          })


setAs("BlastRestrictOptions", "character", function(from) {
  slots <- .getSetSlots(from)
  out <- Map(function(x, name) {
    paste0("-", name, " ", x)
  }, slots, names(slots))
  paste(out, collapse = " ")
})
