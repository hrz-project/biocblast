.bdb_flag <- function(param, slotname) {
  svalue <- slot(param, slotname)
  if (!.empty(svalue) && svalue) paste0("-", slotname) else ""
}

.bdb_char <- function(param, slotname) {
  slotvalue <- slot(param, slotname)
  if (.empty(slotvalue)) {
    ""
  } else {
    paste0("-", slotname, " ", slotvalue)
  }
}

.dbtype <-      function(param) .bdb_char(param, "dbtype")
.dbtitle  <-    function(param) .bdb_char(param, "title")
.dbseqids <-    function(param) .bdb_flag(param, "parse_seqids")
.dbhashindex <- function(param) .bdb_flag(param, "hash_index")
.dbout <-       function(param) .bdb_char(param, "out")
.db_slots <- list(.dbtype, .dbtitle, .dbseqids, .dbhashindex, .dbout)

.makeblastdb <- function(subject_path, param) {
  svalues <- lapply(.db_slots, function(f) f(param))
  cmd <- paste(svalues, collapse = " ")
  cmd <- sprintf("makeblastdb -in %s %s", subject_path, cmd)
  .run_cmd(cmd)
}

setGeneric(".checkDbParam", function(subject, param) {
  standardGeneric(".checkDbParam")
})

setMethod(".checkDbParam", c("DNAStringSet", "BlastDbParam"),
          function(subject, param) {
            if (!(param@dbtype == "nucl"))
              stop("dbtype should be 'nucl'")
          })

setMethod(".checkDbParam", c("DNAStringFile", "BlastDbParam"),
          function(subject, param) {
            if (!(param@dbtype == "nucl"))
              stop("dbtype should be 'nucl'")
          })

setMethod(".checkDbParam", c("AAStringSet", "BlastDbParam"),
          function(subject, param) {
            if (!(param@dbtype == "prot"))
              stop("dbtype should be 'prot'")
          })

setMethod(".checkDbParam", c("AAStringFile", "BlastDbParam"),
          function(subject, param) {
            if (!(param@dbtype == "prot"))
              stop("dbtype should be 'prot'")
          })

.newBlastDb <- function(classname, param) {
  obj <- new(classname, param = param)
  if (validObject(obj)) obj
}

setValidity("BlastDbDNA", function(object) {
    msg <- .messenger()
    if (!(object@param@dbtype == "nucl")) msg("dbtype should be 'nucl'")
    if (.empty(msg())) TRUE else msg()
})

setValidity("BlastDbAA", function(object) {
    msg <- .messenger()
    if (!(object@param@dbtype == "prot")) msg("dbtype should be 'prot'")
    if (.empty(msg())) TRUE else msg()
})


setMethod("blastDb", c("DNAStringFile", "BlastDbParam"),
          function(subject, param, ...) {
            .checkDbParam(subject, param)
            .makeblastdb(path(subject), param)
            if (.empty(param@out)) param@out <- path(subject)
            .newBlastDb("BlastDbDNA", param)
          })

setMethod("blastDb", c("DNAStringSet", "BlastDbParam"),
          function(subject, param, ...) {
            blastDb(as(subject, "DNAStringFile"), param)
          })

setMethod("blastDb", c("AAStringFile", "BlastDbParam"),
          function(subject, param, ...) {
            .stopImpl()
            .checkDbParam(subject, param)
            .newBlastDb("BlastDbAA", param)
          })

setMethod("blastDb", c("AAStringSet", "BlastDbParam"),
          function(subject, param, ...) {
            .stopImpl()
            .checkDbParam(subject, param)
            .newBlastDb("BlastDbAA", param)
          })

setMethod("show", "BlastDb",
          function(object) {
            cat("Blast Database object\n")
            show(object@param)
          })
