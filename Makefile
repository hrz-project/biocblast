
version = $(shell grep '^Version' DESCRIPTION | sed 's|^.*: \(.*\)$$|\1|g')
PKG = biocblast_$(version).tar.gz

.PHONY: check install build install-build vignettes

bioc-check: build
	Rscript -e "BiocCheck::BiocCheck(\"$(PKG)\")"

check: build
	R CMD check $(PKG)

install-build: build
	R CMD INSTALL --build $(PKG)

publish: build
	Rscript -e "drat::insertPackages(\"$(PKG)\")"

vignettes:
	$(MAKE) -c vignettes

build:
	@rm -f $(PKG)
	R CMD build .
